CC = g++

EXEC_NAME=pgm3

LDFLAGS = 
CFLAGS = -Wall

HEADERS = $(wildcard src/*.h src/model/*.h)
SRC = $(wildcard src/*.cpp src/model/*.cpp)
OBJ = $(SRC:.cpp=.o)

all: $(OBJ)
	@echo Linking to make executable
	$(CC) -o $(EXEC_NAME) $(OBJ) $(LDFLAGS)

$(OBJ): $(SRC) $(HEADERS)
	@echo Compiling
	$(CC) $(CFLAGS) -c $(subst .o,.cpp,$@) -o $@

clean:
	@echo Cleaning
	rm -rf $(OBJ) $(EXEC_NAME) $(EXEC_NAME).exe
