//
// Created by djsha on 11/29/2021.
//

#ifndef PGM3_SEMESTER_H
#define PGM3_SEMESTER_H

#include "model/CsciCourse.h"
#include "model/BiobCourse.h"
#include "model/EgenCourse.h"

#include <vector>
using std::vector;

#include <fstream>
using std::ifstream;

class Semester {
private:
  vector<const vector<const void *> *> courses;
  vector<const CsciCourse *> csciCourses;
  vector<const BiobCourse *> biobCourses;
  vector<const EgenCourse *> egenCourses;

public:
  Semester();

  void readLine(ifstream &stream);

  const vector<const vector<const void *> *> &getCourses() const;
};


#endif //PGM3_SEMESTER_H
