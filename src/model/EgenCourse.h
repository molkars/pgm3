//
// Created by djsha on 11/29/2021.
//

#ifndef PGM3_EGENCOURSE_H
#define PGM3_EGENCOURSE_H


#include "Course.h"

class EgenCourse : public Course {
public:
  EgenCourse(string professor, string title, size_t number);

  string toString() const override;
};


#endif //PGM3_EGENCOURSE_H
