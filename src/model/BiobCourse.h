//
// Created by djsha on 11/29/2021.
//

#ifndef PGM3_BIOBCOURSE_H
#define PGM3_BIOBCOURSE_H


#include "Course.h"

class BiobCourse : public Course {
public:
  BiobCourse(size_t number, string title, string professor);

  string toString() const override;
};


#endif //PGM3_BIOBCOURSE_H
