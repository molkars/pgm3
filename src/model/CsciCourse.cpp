//
// Created by djsha on 11/29/2021.
//

#include "CsciCourse.h"

CsciCourse::CsciCourse(string title, size_t number, string professor)
  : Course(CSCI, std::move(professor), std::move(title), number) {}

string CsciCourse::toString() const {
  stringstream ss;
  ss << type << "-" << title << "-" << number << "-" << professor;
  return ss.str();
}