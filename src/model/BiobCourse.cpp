//
// Created by djsha on 11/29/2021.
//

#include "BiobCourse.h"

#include <utility>

BiobCourse::BiobCourse(size_t number, string title, string professor)
  : Course(BIOB, std::move(professor), std::move(title), number) {}

string BiobCourse::toString() const {
  stringstream ss;
  ss << type << "-" << number << "-" << title << "-" << professor;
  return ss.str();
}