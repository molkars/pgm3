//
// Created by djsha on 11/29/2021.
//

#include "Course.h"

Course::Course(CourseType type, string professor, string title, size_t number)
  : type{type}, professor{std::move(professor)}, title{std::move(title)}, number{number} {}

CourseType Course::getType() const { return type; }
string Course::getProfessor() const { return professor; }
string Course::getTitle() const { return title; }
size_t Course::getNumber() const { return number; }

string Course::toString() const {
  stringstream ss;
  ss << "Course: " << number << " " << title << " " << professor;
  return ss.str();
}

istream &operator>>(istream &is, CourseType &course) {
  string type;
  is >> type;
  course = stoct(type);
  return is;
}

ostream &operator<<(ostream &os, CourseType course) {
  os << COURSE_TYPE_STRING[course];
  return os;
}

CourseType stoct(const string& str) {
  for (int i = 0; i < CourseType::COUNT; i++)
    if (str == COURSE_TYPE_STRING[i])
      return static_cast<CourseType>(i);
  return COUNT;
}