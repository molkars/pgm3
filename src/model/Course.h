//
// Created by djsha on 11/29/2021.
//

#ifndef PGM3_COURSE_H
#define PGM3_COURSE_H

#include <string>

using std::string;

typedef enum CourseType {
  EGEN,
  BIOB,
  CSCI,
  COUNT,
} CourseType;

#include <istream>
using std::istream;

#include <ostream>
using std::ostream;

istream &operator>>(istream &is, CourseType &type);
ostream &operator<<(ostream &os, CourseType type);

#include <array>
using std::array;

// This IS a global, but it's a constant.
// I.E. ignore it, it's just as good as using the define macro
const array<const char *, CourseType::COUNT> COURSE_TYPE_STRING{
  "EGEN",
  "BIOB",
  "CSCI",
};

#include <algorithm>

// String to CourseType
CourseType stoct(const string& str);

#include <string>
using std::string;

#include <ostream>
using std::ostream;

#include <sstream>
using std::stringstream;

class Course {
protected:
  const CourseType type;
  const string professor;
  const string title;
  const size_t number;

  Course(CourseType type, string professor, string title, size_t number);

public:
  ~Course() = default;
  CourseType getType() const;
  string getProfessor() const;
  string getTitle() const;
  size_t getNumber() const;

  virtual string toString() const;
};


#endif //PGM3_COURSE_H
