//
// Created by djsha on 11/29/2021.
//

#include "EgenCourse.h"

#include <utility>

EgenCourse::EgenCourse(string professor, string title, size_t number)
  : Course(EGEN, std::move(professor), std::move(title), number) {}

string EgenCourse::toString() const {
  stringstream ss;
  ss << type << "-" << professor << "-" << title << "-" << number;
  return ss.str();
}