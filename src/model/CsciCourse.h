//
// Created by djsha on 11/29/2021.
//

#ifndef PGM3_CSCICOURSE_H
#define PGM3_CSCICOURSE_H


#include "Course.h"

class CsciCourse : public Course {
public:
  CsciCourse(string title, size_t number, string professor);

  string toString() const override;
};


#endif //PGM3_CSCICOURSE_H
