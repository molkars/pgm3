//
// Created by djsha on 11/29/2021.
//

#include "Semester.h"

#include <string>
#include <iostream>

using std::getline;

Semester::Semester() : courses{COUNT}, csciCourses(), biobCourses(), egenCourses() {
  // Cast each list to a vector<const void *> and then put it in the courses list
  courses[CSCI] = reinterpret_cast<const vector<const void *> *>(&csciCourses);
  courses[BIOB] = reinterpret_cast<const vector<const void *> *>(&biobCourses);
  courses[EGEN] = reinterpret_cast<const vector<const void *> *>(&egenCourses);
}

const vector<const vector<const void *> *> &Semester::getCourses() const { return courses; }

void Semester::readLine(ifstream &stream) {
  string subject, num, title, professor;

  getline(stream, subject, ' ');
  getline(stream, num, ',');
  getline(stream, title, ',');
  getline(stream, professor, '\n');

  CourseType type = stoct(subject);
  size_t courseNumber = std::stol(num);

  switch (type) {
    case CourseType::CSCI:
      csciCourses.push_back(new CsciCourse{title, courseNumber, professor});
      break;
    case CourseType::EGEN:
      egenCourses.push_back(new EgenCourse{professor, title, courseNumber});
      break;
    case CourseType::BIOB:
      biobCourses.push_back(new BiobCourse{courseNumber, title, professor});
      break;
    case CourseType::COUNT:
      throw std::runtime_error("Error: Unknown course type: " + subject);
  }
}