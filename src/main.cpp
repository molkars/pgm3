//
// Created by djsha on 11/29/2021.
//

#include <fstream>

using std::ifstream;

#include <iostream>

using std::cerr;
using std::cout;
using std::endl;

#include <vector>

using std::vector;

#include <string>
#include "model/Course.h"
#include "model/CsciCourse.h"
#include "model/EgenCourse.h"
#include "model/BiobCourse.h"
#include "Semester.h"

using std::string;
using std::cin;

int main() {
  ifstream csv{"classes.csv"};
  if (!csv.is_open()) {
    cerr << "Error opening file" << std::endl;
    return 1;
  }

  Semester semester{};

  while (csv.good()) semester.readLine(csv);

  char action;
  do {
    cout << "Choices:" << endl;
    cout << "n - print class with the given number" << endl;
    cout << "t - print all classes of the given subject" << endl;
    cout << "q - quit" << endl;
    cout << "Action:";

    cin >> action;
    switch (action) {
      case 'Q':
      case 'q':
        cout << "Quitting" << endl;
        break;

      case 'N':
      case 'n': {
        cout << "Enter class type and class number (e.g. CSCI 107): ";
        CourseType subject;
        size_t number;
        cin >> subject >> number;

        if (subject == COUNT) {
          cerr << "Unknown course type" << endl;
          break;
        }

        const Course *course = nullptr;

        // Get a pointer the courses for the subject
        const vector<const void *> *subjCourses = semester.getCourses()[subject];

        // Dereference the vector and loop through it
        for (auto c: *subjCourses) {
          // Change the void pointer back into a course pointer
          auto *cs = static_cast<const Course *>(c);
          if (cs->getNumber() == number) {
            course = cs;
            break;
          }
        }

        if (course == nullptr)
          cout << "No class found matching (" << subject << " " << number << ")" << endl;
        else
          cout << course->toString() << endl;
        cout << endl;
        break;
      }

      case 'T':
      case 't': {
        cout << "Enter class type (e.g. CSCI): ";
        CourseType type;
        cin >> type;

        if (type == COUNT) {
          cerr << "Unknown course type" << endl;
          break;
        }

        // Get the vector of courses for the subject
        const vector<const void *> *subjCourses = semester.getCourses()[type];
        // Loop though the courses and print them
        std::for_each(subjCourses->begin(), subjCourses->end(), [&](const void *c) {
          cout << static_cast<const Course *>(c)->toString() << endl;
        });

        break;
      }

      default: {
        cerr << "Unknown action: " << action << endl;
        break;
      }
    }
  } while (action != 'q');

  return 0;
}